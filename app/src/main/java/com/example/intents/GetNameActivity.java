package com.example.intents;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class GetNameActivity extends AppCompatActivity {

    private EditText nameEditText;
    private Button buttonGetName, buttonCancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_name);

        nameEditText = findViewById(R.id.nameEditText);

        buttonGetName = findViewById(R.id.buttonGetName);
        buttonCancel = findViewById(R.id.cancelButton);
    }

    public void onClick(View view)
    {
        if(view == buttonGetName)
        {
            Intent intent = new Intent();
            intent.putExtra("name", nameEditText.getText().toString());
            setResult(RESULT_OK,intent);
            finish();
            return;
        }

        if(view == buttonCancel)
        {
            setResult(RESULT_CANCELED);
            finish();
            return;
        }
    }
}
