package com.example.intents;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    static final int SELECT_PHONE_NUMBER = 0;
    static final int GET_NAME = 1;

    private static final String NAME_KEY = "com.example.intents.name";
    private final static String DEFAULT_NAME = "Unknown";

    private SharedPreferences mPreferences;
    private String sharedPrefFile = "com.example.android.hellosharedprefs";
    private String name;

    private EditText nameEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mPreferences = getSharedPreferences(sharedPrefFile, MODE_PRIVATE);

        name = mPreferences.getString(NAME_KEY, DEFAULT_NAME);

        nameEditText = findViewById(R.id.nameEditText);
        nameEditText.setText(name);
    }

    public void sendMailClick(View view) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_SUBJECT, "my subject");
        intent.putExtra(Intent.EXTRA_TEXT, "my message");
        intent.putExtra(Intent.EXTRA_EMAIL, new String[] {"miolu@generalmagic.com"});
        intent.setType("text/plain");

        // without chooser
        if(intent.resolveActivity(getPackageManager()) != null)
            startActivity(intent);

        // with chooser
        // Intent chooser = Intent.createChooser(intent, "My Title");
        // if(intent.resolveActivity(getPackageManager()) != null)
        //      startActivity(chooser);
    }

    public void openMap(View view) {
        Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.google.com"));
        startActivity(i);

        //open webpage
        //Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.google.com"));
        //startActivity(i);
    }

    public void getName(View view) {
        Intent intent = new Intent(this, GetNameActivity.class);
        startActivityForResult(intent, GET_NAME);
    }

    public void pickContact(View view) {
        Intent i=new Intent(Intent.ACTION_PICK);
        i.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
        startActivityForResult(i, SELECT_PHONE_NUMBER);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        Log.i("myapp", "ON ACTIVITY RESULT SUCCEEDED");

        if(resultCode == RESULT_CANCELED)
            Log.i("myapp", "CANCELED");

        switch(requestCode)
        {
            case GET_NAME:
            {
                if(resultCode == RESULT_OK)
                {
                    String name = data.getStringExtra("name");
                    Toast.makeText(this, "Your name: " + name, Toast.LENGTH_SHORT).show();
                }
                else
                    Toast.makeText(this, "Name was not provided!!! ", Toast.LENGTH_SHORT).show();

                break;
            }
            case SELECT_PHONE_NUMBER:
            {
                if(resultCode == RESULT_OK)
                {
                    // Get the URI and query the content provider for the phone number
                    Uri contactUri = data.getData();

                    Toast.makeText(this, "\"CONTACT: \" + contactUri.toString()", Toast.LENGTH_LONG).show();

                    String[] projection = new String[]{ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME, ContactsContract.CommonDataKinds.Phone.NUMBER};

                    Cursor cursor = getContentResolver().query(contactUri, projection, null, null, null);

                    // If the cursor returned is valid, get the phone number
                    if (cursor != null && cursor.moveToFirst())
                    {
                        int numberIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
                        String number = cursor.getString(numberIndex);

                        numberIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
                        String displayName = cursor.getString(numberIndex);

                        Toast.makeText(this, "DISPLAY NAME: " + displayName +"\nPHONE NUMBER: " + number, Toast.LENGTH_LONG).show();
                    }

                    cursor.close();
                }
            }
        }
    }

    public void sharedPrefsApply(View view) {
        SharedPreferences.Editor sharedPrefEditor = mPreferences.edit();
        sharedPrefEditor.putString(NAME_KEY, nameEditText.getText().toString());
        sharedPrefEditor.apply();
    }

    public void sharedPrefsReset(View view) {
        SharedPreferences.Editor sharedPrefEditor = mPreferences.edit();
        sharedPrefEditor.clear();
        sharedPrefEditor.apply();

        nameEditText.setText(DEFAULT_NAME);
    }
}
